FROM	hseeberger/scala-sbt:17.0.1_1.6.2_3.1.1

RUN     apt-get -y update && \
	    apt-get -y install clang libcsfml-dev openssh-client
