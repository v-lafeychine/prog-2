NAME		=	prog-2
SCALA_VERSION	=	3.1.1

CLIENT		=	prog-client
SERVER		=	prog-server

CP		=	cp
RM		=	rm
SBT		=	sbt


all:		$(CLIENT) $(SERVER)

$(CLIENT):
		echo "1" | $(SBT) nativeLink
		$(CP) target/scala-$(SCALA_VERSION)/$(NAME)-out $@

$(SERVER):
		echo "2" | $(SBT) nativeLink
		$(CP) target/scala-$(SCALA_VERSION)/$(NAME)-out $@

clean:
		$(SBT) clean
		$(RM) $(CLIENT) $(SERVER)

.NOTPARALLEL:
.PHONY:		all clean
