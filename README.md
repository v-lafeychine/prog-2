# Projet programmation 2

## Building the project

Firstly, to be able to fetch [https://github.com/lafeychine/scala-native-sfml](SFML dependency), you must add authentication token.

To do this, you add either `GITHUB_TOKEN="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"` before each `sbt` invocation or this into `~/.gitconfig`:
```
[github]
	token = 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
```


### Non-NixOS users

The project depends on the following dependencies:
 - `clang` and `clang++`
 - `openjdk-jre-headless`
 - `libcsfml-dev`
 - `sbt`

Make sure to have clone recursively this project, or:
```
$ cd CSFML && git clone https://github.com/lafeychine/CSFML
```

Then, build and launch the project:
```
$ make
$ ./prog-2
```


### NixOS users

A `nix-shell` is given to easily launch the project on NixOS, simply run:
```
$ nix-shell --run "sbt run"
```


## Documentation

The current documentation is available [https://perso.crans.org/v-lafeychine/](here).

The project uses `ScalaDoc` to generate the documentation:
```
$ sbt doc                     // non-NixOS users
$ nix-shell --run "sbt doc"   // NixOS users
```


## Testing the project

The project uses `JUnit` to test the project:
```
$ sbt test                    // non-NixOS users
$ nix-shell --run "sbt test"  // NixOS users
```
