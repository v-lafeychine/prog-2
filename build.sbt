name := "prog-2"
scalaVersion := "3.1.1"

enablePlugins(ScalaNativePlugin)

nativeLTO := "thin"
nativeMode := "release-full"

nativeLinkingOptions := Seq(
    "-lcsfml-audio",
    "-lcsfml-graphics",
    "-lcsfml-network",
    "-lcsfml-system",
    "-lcsfml-window"
)

// SFML dependency
githubSuppressPublicationWarning := true
githubTokenSource := TokenSource.Environment("GITHUB_TOKEN") || TokenSource.GitConfig("github.token")

resolvers += Resolver.githubPackages("lafeychine")
libraryDependencies += "io.github.lafeychine" %%% "scala-native-sfml" % "0.1.3"

// Testing section
addCompilerPlugin("org.scala-native" % "junit-plugin" % "0.4.4" cross CrossVersion.full)
libraryDependencies += "org.scala-native" %%% "junit-runtime" % "0.4.4"
testOptions += Tests.Argument(TestFrameworks.JUnit, "-a", "-s", "-v")

// Documentation section
Compile / doc / scalacOptions ++= Seq("-d", "docs", "-skip-by-regex:SFML")

// ScalaPB
Compile / PB.targets := Seq(
    scalapb.gen(
        grpc = false,
        lenses = false
    ) -> (Compile / sourceManaged).value / "scalapb"
)
