{ pkgs ? import <nixpkgs> { } }:

pkgs.stdenv.mkDerivation (rec {
  name = "prog-2";
  buildInputs = with pkgs; [ clang csfml sbt which ];
})
