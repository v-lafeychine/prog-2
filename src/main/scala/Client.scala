package Client

import sfml.graphics.RenderWindow
import sfml.window.{VideoMode, Window}

@main def main() =
    val videoMode = VideoMode(1024, 768, 32)
    val window = RenderWindow(videoMode, "Projet", Window.WindowStyle.DefaultStyle)

    window.framerateLimit = 20
    window.verticalSync = true

    Engine.GameEngine(PokemonGame(), window)
