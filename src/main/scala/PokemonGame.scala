package Client

import Engine.Game

import Scene.Menu.MenuScene

/** The instance of the Pokemon game.
  */
class PokemonGame() extends Game(MenuScene())
