/** Engine based on [[https://www.sfml-dev.org/ SFML library]], inspired by [[https://unity.com Unity]]
  *
  * The engine is composed of:
  *   - [[Engine.Objects Game objects]]: Containers for [[Engine.Components components]] and [[Engine.Objects game sub-objects]].
  *   - [[Engine.Components Components]]: Attached to a [[Engine.Objects game object]], giving it logical properties.
  *
  * A [[Engine.Objects.Scene scene]] is a view of a game, like game level or battle's menu, containing [[Engine.Objects game objects]].
  */
package object Engine

package Engine:
    /** [[Engine.Components Components]] are attached to a [[Engine.Objects game object]], giving it logical properties.
      *
      * Every component is defined as a trait: Developers must extends them as classes.
      *
      * {{{
      * class ExampleComponent(exampleParam: Int) extends Component:
      *     def foo(): Unit = ()
      *
      * val gameObject = GameObject()
      *
      * val component = gameObject.addComponent[ExampleComponent](42)
      * component.foo()
      * }}}
      */
    package object Components

    /** [[Engine.Objects Game object]] are containers for [[Engine.Components components]] and [[Engine.Objects game sub-objects]].
      *
      * Helper objects are available for recurring tasks.
      *
      * {{{
      * class ExampleObject(exampleParam: Int) extends GameObject:
      *     def foo(): Unit = ()
      *
      * val gameObject = ExampleObject(42)
      * gameObject.foo()
      * }}}
      */
    package object Objects
