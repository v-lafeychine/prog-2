package Engine

import Objects.Scene

/** Defines what an instance of a game must have.
  */
trait Game(val primaryScene: Scene)
