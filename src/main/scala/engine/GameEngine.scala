package Engine

import Components.{EventListener, KeyListener, MonoBehaviour, MouseCollider, Renderer}
import ResponseStatus.{FailureTransfer, Error, Success}
import Objects.Scene

import protocol.{ClientData, Request, Response, ResponseData, ServerData}

import scala.collection.mutable.ListBuffer

import scalanative.unsigned.{UShort, UnsignedRichInt}

import scalapb.GeneratedMessage

import sfml.graphics.{Color, RenderWindow}
import sfml.network.{IpAddress, SocketStatus, TcpSocket}
import sfml.system.Time
import sfml.window.{Event, Mouse}

/** Main instance of the game engine.
  *
  * @param game
  *   Instance of a game.
  * @param window
  *   Initialized `window`.
  * @constructor
  *   Instantiate the engine with a `game` and initialized `window`.
  * @note
  *   Instantiating the engine will never return until the game window closes.
  */
class GameEngine(game: Game, val window: RenderWindow):
    private var _currentScene: Scene = game.primaryScene

    private var socket: Option[TcpSocket] = None
    private var socketCallback: ListBuffer[(Long, ResponseStatus => Unit)] = ListBuffer()
    private var socketMessageId: Int = 0

    def currentScene: Scene = _currentScene

    while window.isOpen() do
        window.clear(Color.Black())

        pollEvent()
        pollNetwork()

        _currentScene.recursiveComponents[Renderer]().filter(_.isActive).sorted.foreach(_.draw(window))
        _currentScene.recursiveComponents[MonoBehaviour]().filter(_.isActive).sorted.foreach(_.load(this))

        window.display()

    /** Change the current scene to `scene`, from next frame.
      *
      * @param scene
      *   Scene to load.
      */
    def changeScene(scene: Scene): Unit =
        _currentScene = scene

    def connect(address: IpAddress, remotePort: UShort): Either[String, Unit] =
        val socketAttempt = TcpSocket()

        /* Force reload window, before connection */
        _currentScene.recursiveComponents[Renderer]().filter(_.isActive).sorted.foreach(_.draw(window))
        window.display()

        socket match
            case Some(_) => Left("The server is already connected")
            case None =>
                if socketAttempt.connect(address, remotePort, Time(3 * 1000000)) == SocketStatus.Done(()) then
                    socketAttempt.blocking = false
                    socket = Some(socketAttempt)
                    Right(())
                else Left("Cannot connect to the server")

    def sendRequest(request: Request, callback: ResponseStatus => Unit = _ => ()): Unit =
        socket match
            case None => callback(FailureTransfer("The server isn't connected"))
            case Some(socket) =>
                socketMessageId = socketMessageId + 1;
                socket.sendPartial(ClientData(socketMessageId).withRequest(request).toByteArray) match
                    case SocketStatus.Done(_) => socketCallback += ((socketMessageId, callback))
                    case _                    => callback(FailureTransfer("The data wasn't correctly sent"))

    private def pollNetwork(): Unit =
        socket match
            case None => ()
            case Some(socket) =>
                while true do
                    socket.receive() match
                        case SocketStatus.Disconnected() => ()

                        case SocketStatus.Done(data) =>
                            try
                                ServerData.parseFrom(data).dataType match
                                    case ServerData.DataType.Event(event) =>
                                        _currentScene.recursiveComponents[EventListener]().filter(_.isActive).foreach(_.handleEvent(this, event))

                                    case ServerData.DataType.ResponseData(ResponseData(id, responseData, _)) =>
                                        socketCallback = socketCallback.dropWhile((key, _) => key < id)
                                        val (_, callback) = socketCallback.head

                                        if responseData.isEmpty then println("[WARN] The data wasn't correctly recieved")
                                        else if responseData.isError then callback(Error(responseData.error.get))
                                        else callback(Success(responseData.response.get))

                                        socketCallback = socketCallback.drop(1)

                                    case e => println("[WARN] The data wasn't correctly recieved: " + e)
                            catch case e => println("[WARN] The data wasn't correctly recieved: " + e)

                        case _ => return

    private def pollEvent(): Unit =
        for event <- window.pollEvent() do
            event match
                case Event.Closed() =>
                    socket.map(_.close())
                    window.close()

                case event @ Event.MouseMoved(_, _) =>
                    _currentScene
                        .recursiveComponents[MouseCollider]()
                        .filter(_.isActive)
                        .foreach(_.handleMouseEvent(this, event))

                case event @ Event.MouseButtonPressed(_, _, _) =>
                    _currentScene
                        .recursiveComponents[MouseCollider]()
                        .filter(_.isActive)
                        .foreach(_.handleMouseEvent(this, event))

                case Event.KeyPressed(key, _, _, _, _) =>
                    _currentScene
                        .recursiveComponents[KeyListener]()
                        .filter(_.isActive)
                        .foreach(_.handleKeyEvent(this, key))

                case _ => ()
