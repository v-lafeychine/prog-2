package Engine

enum ResponseStatus:
    case FailureTransfer(reason: String)
    case Error(error: protocol.Error)
    case Success(response: protocol.Response)
