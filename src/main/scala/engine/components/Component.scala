package Engine
package Components

import Objects.GameObject

/** A component is attached to a [[Engine.Objects.GameObject game object]], giving it logic properties.
  */
trait Component:
    /** The [[Engine.Objects.GameObject game object]] this component is attached to. */
    var gameObject: GameObject = null

    /** Select if the component will receive [[Engine.GameEngine engine]]'s updates */
    var active: Boolean = true

    private[Engine] def isActive: Boolean =
        active && gameObject.isActive

    /** Syntaxic-sugar for remove this component of the [[Engine.Objects.GameObject game object]] */
    def destroy(): Unit =
        gameObject.removeComponent(this)
