package Engine
package Components

trait EventListener extends Component:
    def handleEvent(engine: GameEngine, event: protocol.Event): Unit
