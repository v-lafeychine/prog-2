package Engine
package Components

import sfml.window.Keyboard.Key

trait KeyListener extends Component:
    def handleKeyEvent(engine: GameEngine, event: Key): Unit
