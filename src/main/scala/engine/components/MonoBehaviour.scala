package Engine
package Components

trait MonoBehaviour(val priority: Int = 0) extends Ordered[MonoBehaviour], Component:
    private[Engine] var isInitialized: Boolean = false

    private[Engine] def load(engine: GameEngine): Unit =
        if !isInitialized then { init(engine); isInitialized = true }
        update(engine)

    def init(engine: GameEngine): Unit
    def update(engine: GameEngine): Unit

    def compare(rhs: MonoBehaviour) = priority - rhs.priority
