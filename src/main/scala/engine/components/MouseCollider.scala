package Engine
package Components

import sfml.graphics.FloatRect
import sfml.window.Mouse
import sfml.window.Event.{MouseButtonPressed, MouseMoved}

trait MouseCollider(bounds: FloatRect) extends Component:
    private var isHovering = false

    def handleMouseEvent(engine: GameEngine, event: MouseMoved): Unit =
        val MouseMoved(x, y) = event

        if bounds.contains(x, y) then
            if !isHovering then
                onMouseEnter(engine)
                isHovering = true
        else if isHovering then
            onMouseExit(engine)
            isHovering = false

    def handleMouseEvent(engine: GameEngine, event: MouseButtonPressed): Unit =
        event match
            case MouseButtonPressed(Mouse.Button.Left, x, y) =>
                if bounds.contains(x, y) then onMouseClick(engine)
            case _ => ()

    def onMouseEnter(engine: GameEngine): Unit

    def onMouseClick(engine: GameEngine): Unit

    def onMouseExit(engine: GameEngine): Unit
