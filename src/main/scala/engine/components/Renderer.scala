package Engine
package Components

import sfml.graphics.{Color, FloatRect, RenderWindow}

trait Renderer extends Ordered[Renderer], Component:
    val bounds = FloatRect()

    val color: Color

    var layer: RendererLayer

    def compare(rhs: Renderer) = layer.depth - rhs.layer.depth

    private[Engine] def draw(window: RenderWindow): Unit
