package Engine
package Objects

import Components.{MouseCollider, RendererLayer}

import sfml.graphics.{FloatRect, Texture}

class Button(texture: Texture, layer: RendererLayer) extends GameObject:
    val sprite = addComponent[Sprite](texture, layer)

    addComponent[ButtonUI](bounds)

    def bounds: FloatRect = sprite.bounds

    private class ButtonUI(bounds: FloatRect) extends MouseCollider(bounds):
        def onMouseEnter(engine: GameEngine) =
            sprite.color.r = 200.toByte
            sprite.color.g = 200.toByte
            sprite.color.b = 200.toByte

        def onMouseClick(engine: GameEngine) = ()

        def onMouseExit(engine: GameEngine) =
            sprite.color.r = 255.toByte
            sprite.color.g = 255.toByte
            sprite.color.b = 255.toByte
