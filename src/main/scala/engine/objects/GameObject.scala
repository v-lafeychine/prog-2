package Engine
package Objects

import scala.collection.mutable.ListBuffer
import scala.quoted.*
import scala.reflect.*

import Components.Component

import sfml.system.Vector2f

class GameObject(val pos: Vector2f = Vector2f(0, 0), var scale: Float = 1.0f):
    private val _children: ListBuffer[GameObject] = ListBuffer()
    private val _components: ListBuffer[Component] = ListBuffer()
    private var _parent: Option[GameObject] = None

    /** Select if the gameObject and [[Engine.Components.Component components]] will receive [[Engine.GameEngine engine]]'s updates */
    var active: Boolean = true

    private[Engine] def isActive: Boolean = _parent match
        case Some(parent) => active && parent.isActive
        case None         => active

    def addChild[T <: GameObject](child: T): T =
        child._parent = Some(this)
        _children += child
        return child

    def recursiveChildren(): ListBuffer[GameObject] =
        _children.flatMap(child => child +: child.recursiveChildren())

    def moveTo(pos: Vector2f): this.type = { this.pos := pos; this }

    inline def translate(translation: Vector2f, frame: Int = 1, callback: () => Unit = () => ()): Unit =
        addComponent[Translation](translation, frame, callback)

    def parent: GameObject = _parent.get

    def removeChild[T <: GameObject](child: T): Unit =
        _children -= child

    private[Engine] def globalPos: Vector2f = _parent match
        case Some(parent) => pos * globalScale + parent.globalPos
        case None         => pos

    def globalScale: Float = _parent match
        case Some(parent) => scale * parent.globalScale
        case None         => scale

    inline def addComponent[T <: Component](inline varargs: Any*): T =
        ${ GameObject.constructComponent[T]('this, 'varargs) }

    def getComponent[T <: Component]()(using TypeTest[Component, T]): T =
        return getComponents()(0)

    def getComponents[T <: Component]()(using TypeTest[Component, T]): ListBuffer[T] =
        return _components
            .filter(_ match
                case x: T => true
                case _    => false
            )
            .asInstanceOf[ListBuffer[T]]

    inline def recursiveComponents[T <: Component](): ListBuffer[T] =
        getComponents[T]() ++
            recursiveChildren().flatMap(_.getComponents[T]())

    def removeComponent(component: Component): Unit =
        _components -= component

object GameObject:
    def constructComponent[T <: Component: Type](gameObject: Expr[GameObject], varargs: Expr[Seq[Any]])(using
        Quotes
    ): Expr[T] =
        import quotes.reflect.*

        val Varargs(args) = varargs
        val argsTerm = args.map(_.asTerm).toList

        val componentASTConstructor =
            Apply(Select(New(TypeTree.of[T]), TypeRepr.of[T].typeSymbol.primaryConstructor), argsTerm).asExprOf[T]
        '{
            val component = $componentASTConstructor;
            component.gameObject = $gameObject;
            $gameObject._components += component;
            component
        }
