package Engine
package Objects

import scala.collection.mutable.ListBuffer

import Components.Component

trait Scene extends GameObject:
    def addGameObject[T <: GameObject](gameObject: T): T =
        return addChild(gameObject)
