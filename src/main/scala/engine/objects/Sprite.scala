package Engine
package Objects

import Components.{Renderer, RendererLayer}

import sfml.graphics.{Color, FloatRect, IntRect, RenderWindow, Sprite as SFMLSprite, Texture}
import sfml.system.{Vector2i, Vector2f}

class Sprite(private var _texture: Texture, var layer: RendererLayer) extends Renderer:
    private var sprite = SFMLSprite(_texture)

    var animationNumber = 0

    val color = Color()

    val rect = IntRect()

    def setTexture(texture: Texture) = {
        _texture = texture
        sprite = SFMLSprite(_texture)
    }

    private[Engine] def draw(window: RenderWindow) =
        if rect != IntRect() then sprite.textureRect = IntRect(rect.left + rect.width * animationNumber, rect.top, rect.width, rect.height)

        sprite.color = color
        sprite.position = gameObject.globalPos
        sprite.scale = Vector2f(gameObject.globalScale, gameObject.globalScale)
        window.draw(sprite)

        bounds := sprite.globalBounds
