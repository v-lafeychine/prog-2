package Engine
package Objects

import Components.{Renderer, RendererLayer}

import sfml.graphics.{Color, FloatRect, Font, IntRect, RenderWindow, Text as SFMLText}
import sfml.system.{Vector2i, Vector2f}

class Text(font: Font, var msg: String, var layer: RendererLayer) extends Renderer:
    private val text = SFMLText()

    text.font = font

    val color = Color.Black()

    private[Engine] def draw(window: RenderWindow) =
        text.color = color
        text.position = gameObject.globalPos
        text.string = msg

        window.draw(text)

        bounds := text.globalBounds
