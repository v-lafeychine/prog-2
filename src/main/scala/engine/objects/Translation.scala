package Engine
package Objects

import Components.MonoBehaviour

import sfml.system.Vector2f

class Translation(translation: Vector2f, frame: Int, callback: () => Unit = () => ()) extends MonoBehaviour():
    private val initialPosition = Vector2f()
    private val vector = translation * (1 / frame.toFloat)
    private var currentFrame = 0

    def init(engine: GameEngine): Unit =
        initialPosition := gameObject.pos

    def update(engine: GameEngine): Unit =
        currentFrame = currentFrame + 1

        gameObject.moveTo(initialPosition + (vector * currentFrame))

        if currentFrame == frame then
            gameObject.moveTo(initialPosition + translation)
            callback()
            destroy()
