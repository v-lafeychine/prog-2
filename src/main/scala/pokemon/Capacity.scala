package Pokemon

class Capacity(val name: String, val ctype: Type, val maxUses: Int, private var _usesLeft: Int, val power: Int, val toProtobuf: protocol.Capacity):

    // getter
    def usesLeft: Int = this._usesLeft

    def use(): Unit =
        this._usesLeft = this._usesLeft - 1

    def isUsable: Boolean = this.usesLeft > 0
