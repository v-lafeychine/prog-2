package Pokemon

import protocol.Capacity.*

object CapacityFactory:
    private case class Absorb() extends Capacity("Absorb", Grass, 25, 25, 20, ABSORB)
    private case class Acid() extends Capacity("Acid", Poison, 30, 30, 40, ACID)
    private case class Bubble() extends Capacity("Bubble", Water, 30, 30, 40, BUBBLE)
    private case class BubbleBeam() extends Capacity("Bubble Beam", Water, 20, 20, 65, BUBBLE_BEAM)
    private case class Cachan() extends Capacity("Cachan", Ghost, 25, 25, 40, CACHAN)
    private case class Constrict() extends Capacity("Constrict", Normal, 35, 35, 10, CONSTRICT)
    private case class DragonRage() extends Capacity("Dragon Rage", Dragon, 15, 15, 75, DRAGON_RAGE)
    private case class Ecocup() extends Capacity("Ecocup", Ground, 25, 25, 40, ECOCUP)
    private case class Ember() extends Capacity("Ember", Fire, 25, 25, 40, EMBER)
    private case class Fensfoire() extends Capacity("Fensfoire", Psychic, 25, 25, 40, FENSFOIRE)
    private case class FireSpin() extends Capacity("Fire Spin", Fire, 15, 15, 35, FIRE_SPIN)
    private case class RockThrow() extends Capacity("Rock Throw", Rock, 15, 15, 50, ROCK_THROW)
    private case class Thunder() extends Capacity("Thunder", Electric, 10, 10, 110, THUNDER)
    private case class Twineedle() extends Capacity("Twineedle", Bug, 20, 20, 55, TWINEEDLE)
    private case class VineWhip() extends Capacity("Vine Whip", Grass, 25, 25, 45, VINE_WHIP)

    def apply(name: protocol.Capacity): Capacity = name match
        case ABSORB      => Absorb()
        case ACID        => Acid()
        case BUBBLE      => Bubble()
        case BUBBLE_BEAM => BubbleBeam()
        case CACHAN      => Cachan()
        case CONSTRICT   => Constrict()
        case DRAGON_RAGE => DragonRage()
        case ECOCUP      => Ecocup()
        case EMBER       => Ember()
        case FENSFOIRE   => Fensfoire()
        case FIRE_SPIN   => FireSpin()
        case ROCK_THROW  => RockThrow()
        case THUNDER     => Thunder()
        case TWINEEDLE   => Twineedle()
        case VINE_WHIP   => VineWhip()
        case _           => Absorb()
