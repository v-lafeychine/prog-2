package Pokemon

class Efficiency(val factor: Double)

case object Efficient extends Efficiency(2.0)
case object Neutral extends Efficiency(1.0)
case object NotEfficient extends Efficiency(0.5)
case object Inefficient extends Efficiency(0.0)
