package Pokemon

trait Pokemon(
    val id: Int,
    private var _name: String,
    private var _life: Int,
    val ptype: List[Type],
    pcapacity: List[Capacity],
    baseAttack: Int,
    baseDefense: Int,
    baseSpeed: Int,
    baseLife: Int,
    level: Int
):

    def name: String = this._name
    def life: Int = this._life
    def capacity: List[Capacity] = this.pcapacity

    private def name_=(name: String) = this._name = name
    private def life_=(life: Int) = this._life = life

    def attack: Int = 5 + scala.math.floor(2.0 * this.baseAttack * this.level / 100.0).toInt
    def defense: Int = 5 + scala.math.floor(2.0 * this.baseDefense * this.level / 100.0).toInt
    def speed: Int = 5 + scala.math.floor(2.0 * this.baseSpeed * this.level / 100.0).toInt
    def maxLife: Int = scala.math.floor(2.0 * this.baseSpeed * this.level / 100.0).toInt + 10 + this.level

    def useCapacity(capacity: Capacity, other: Pokemon): Int =
        capacity.use()

        val multiplier = other.ptype.foldLeft(1.0)(_ * capacity.ctype.typeEfficiency(_).factor)

        val damage =
            (((2.0 * (this.level) / 5.0) * (capacity.power) * (this.attack)
                / (other.defense)) / 50.0 + 2.0)
                * (scala.util.Random.between(0.85, 1.0)) * multiplier
                * (if this.ptype.contains(capacity.ctype) then 1.5 else 1.0)

        other.decreaseLife(Math.round(damage.toFloat))

        return Math.round(damage.toFloat)

    def decreaseLife(x: Int): Unit =
        if this.life - x <= 0 then this._life = 0
        else this._life = this.life - x

    def isAlive: Boolean = this.life != 0
