package Pokemon

import protocol.Capacity.*

object PokemonFactory:

    private case class Bulbasaur()
        extends Pokemon(
            1,
            "Bulbasaur",
            11,
            List(Grass, Poison),
            List(
                CapacityFactory(ABSORB),
                CapacityFactory(VINE_WHIP),
                CapacityFactory(CONSTRICT),
                CapacityFactory(BUBBLE)
            ),
            49,
            49,
            45,
            45,
            1
        )
    private case class Charmander()
        extends Pokemon(
            4,
            "Charmander",
            12,
            List(Fire),
            List(
                CapacityFactory(FIRE_SPIN),
                CapacityFactory(EMBER),
                CapacityFactory(ROCK_THROW),
                CapacityFactory(THUNDER)
            ),
            52,
            43,
            65,
            39,
            1
        )
    private case class Squirtle()
        extends Pokemon(
            7,
            "Squirtle",
            11,
            List(Water),
            List(
                CapacityFactory(BUBBLE),
                CapacityFactory(BUBBLE_BEAM),
                CapacityFactory(ROCK_THROW),
                CapacityFactory(ABSORB)
            ),
            48,
            65,
            43,
            44,
            1
        )
    private case class Clefairy()
        extends Pokemon(
            35,
            "Clefairy",
            12,
            List(Psychic, Ice),
            List(
                CapacityFactory(FENSFOIRE),
                CapacityFactory(ECOCUP),
                CapacityFactory(CONSTRICT),
                CapacityFactory(ROCK_THROW)
            ),
            45,
            48,
            35,
            70,
            1
        )
    private case class Kangaskhan()
        extends Pokemon(
            115,
            "Kangaskhan",
            13,
            List(Ghost, Fight),
            List(
                CapacityFactory(CACHAN),
                CapacityFactory(DRAGON_RAGE),
                CapacityFactory(ROCK_THROW),
                CapacityFactory(ACID)
            ),
            95,
            80,
            90,
            105,
            1
        )
    private case class Scyther()
        extends Pokemon(
            123,
            "Scyther",
            12,
            List(Bug, Flying),
            List(
                CapacityFactory(TWINEEDLE),
                CapacityFactory(DRAGON_RAGE),
                CapacityFactory(VINE_WHIP),
                CapacityFactory(ACID)
            ),
            110,
            80,
            105,
            70,
            1
        )

    def apply(name: protocol.Pokemon): Pokemon = name match
        case protocol.Pokemon.BULBASAUR  => Bulbasaur()
        case protocol.Pokemon.CHARMANDER => Charmander()
        case protocol.Pokemon.SQUIRTLE   => Squirtle()
        case protocol.Pokemon.CLEFAIRY   => Clefairy()
        case protocol.Pokemon.KANGASKHAN => Kangaskhan()
        case protocol.Pokemon.SCYTHER    => Scyther()
        case _                           => Bulbasaur()
