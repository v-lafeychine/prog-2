package Pokemon

sealed trait Type(val ordinal: Int, val strong_on: List[Type], val weak_on: List[Type], val inefficient_on: List[Type]):
    def typeEfficiency(ptype: Type): Efficiency =
        if strong_on.contains(ptype) then Efficient
        else if weak_on.contains(ptype) then NotEfficient
        else if inefficient_on.contains(ptype) then Inefficient
        else Neutral

case object Bug extends Type(0, List(Grass, Poison, Psychic), List(Fight, Fire, Flying, Ghost), List())
case object Dragon extends Type(1, List(Dragon), List(), List())
case object Electric extends Type(2, List(Flying, Water), List(Dragon, Electric, Grass), List(Ground))
case object Fight extends Type(3, List(Normal, Rock, Ice), List(Poison, Flying, Bug, Psychic), List(Ghost))
case object Fire extends Type(4, List(Bug, Grass, Ice), List(Dragon, Fire, Rock, Water), List())
case object Flying extends Type(5, List(Bug, Fight, Grass), List(Electric, Rock), List())
case object Ghost extends Type(6, List(Ghost), List(), List(Normal, Psychic))
case object Grass extends Type(7, List(Ground, Rock, Water), List(Bug, Dragon, Fire, Flying, Grass, Poison), List())
case object Ground extends Type(8, List(Electric, Fire, Poison, Rock), List(Bug, Grass), List(Flying))
case object Ice extends Type(9, List(Dragon, Flying, Grass, Ground), List(Ice, Water), List())
case object Normal extends Type(10, List(), List(Rock), List(Ghost))
case object Poison extends Type(11, List(Bug, Grass), List(Poison, Ground, Rock, Ghost), List())
case object Psychic extends Type(12, List(Fight, Poison), List(Psychic), List())
case object Rock extends Type(13, List(Bug, Fire, Flying, Ice), List(Fight, Ground), List())
case object Water extends Type(14, List(Fire, Ground, Rock), List(Dragon, Grass, Water), List())
