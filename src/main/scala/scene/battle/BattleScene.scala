package Scene.Battle

import BattlePokemon.{BattlePokemon, BattleBackPokemon, BattleFrontPokemon}

import Engine.GameEngine
import Engine.Components.{EventListener, MouseCollider}
import Engine.ResponseStatus.{FailureTransfer, Error, Success}
import Engine.Objects.{GameObject, Scene, Sprite}

import PlayerMenu.CapacitySelector.CapacitySelectorOverlay
import PlayerMenu.FightButton

import Pokemon.Trainer

import protocol.{BattleAttackEvent, BattleEndedEvent, Event, SyncToMapRequest, SyncToMapResponse, Request}

import sfml.graphics.Texture

class BattleScene(back: Trainer, front: Trainer, _isAttacker: Boolean) extends GameObject(scale = 4) with Scene:
    private val frontPokemon = BattleFrontPokemon(front.pokemon)
    private val backPokemon = BattleBackPokemon(back.pokemon)
    private var isAttacker = _isAttacker

    addComponent[Sprite](Texture("src/main/resources/battle/background.png"), Layer.Background.layer)
    addComponent[Sprite](Texture("src/main/resources/battle/tiles.png"), Layer.Background.offset(1))
    addComponent[BattleEvent]()

    private var backPokemonSprite = addGameObject(backPokemon.sprite)
    private var backPokemonStatus = addGameObject(backPokemon.status)
    private var backCapacitySelector = addGameObject(backPokemon.overlay)

    private var frontPokemonSprite = addGameObject(frontPokemon.sprite)
    private var frontPokemonStatus = addGameObject(frontPokemon.status)

    private val buttonGroup = addGameObject(GameObject(scale = 0.8))
    private val fightButton = FightButton(backPokemon.overlay)
    buttonGroup.addChild(fightButton.moveTo((125, 185)))

    recursiveComponents[MouseCollider]().foreach(_.active = isAttacker)
    fightButton.active = isAttacker

    private class BattleEvent() extends EventListener:
        private def attack(pokemon: BattlePokemon, damage: Int): Unit =
            gameObject
                .addChild(GameObject())
                .translate(
                    (0, 0),
                    8,
                    () => {
                        pokemon.pokemon.decreaseLife(damage)
                        pokemon.takeDamageAnim()
                        pokemon.status.healthBar.update()
                    }
                )

        def handleEvent(engine: GameEngine, event: Event): Unit =
            event match
                case BattleAttackEvent(attackerName, attackedName, _, damage, _) =>
                    if isAttacker && attackerName == back.name then attack(frontPokemon, damage)
                    else if !isAttacker && attackedName == back.name then attack(backPokemon, damage)
                    else return

                    isAttacker = !(isAttacker)

                    recursiveComponents[MouseCollider]().foreach(_.active = isAttacker)
                    fightButton.active = isAttacker

                case BattleEndedEvent(winnerName, looserName, _) =>
                    if back.name != winnerName && back.name != looserName then return

                    engine.sendRequest(
                        Request().withSyncToMap(SyncToMapRequest()),
                        data =>
                            data match
                                case FailureTransfer(error) => ()
                                case Error(error)           => println("Error: " + error)
                                case Success(response) =>
                                    response match
                                        case SyncToMapResponse(player, characters, _) =>
                                            engine.changeScene(Scene.Map.MapScene(player.get, characters))
                                        case _ => ()
                    )

                case _ => ()
