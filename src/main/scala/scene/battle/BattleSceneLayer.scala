package Scene.Battle

import Engine.Components.RendererLayer

enum Layer(depth: Int):
    val layer = RendererLayer(depth)

    def offset(offset: Int): RendererLayer =
        RendererLayer(layer.depth + offset)

    case Background extends Layer(0)
    case Pokemons extends Layer(10)
    case UI extends Layer(20)
    case Overlay extends Layer(30)
