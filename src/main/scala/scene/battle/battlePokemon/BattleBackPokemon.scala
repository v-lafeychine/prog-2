package Scene.Battle
package BattlePokemon

import Engine.Objects.{GameObject, Sprite}

import PlayerMenu.CapacitySelector.CapacitySelectorOverlay

import Pokemon.Pokemon

import PokemonStatus.{PokemonStatus, PokemonBackStatus}

import sfml.graphics.Texture

class BattleBackPokemon(pokemon: Pokemon) extends BattlePokemon(pokemon):
    val overlay = CapacitySelectorOverlay(pokemon).moveTo((0, -192))

    val sprite: GameObject = GameObject().moveTo((32, 80))
    sprite.addComponent[Sprite](
        Texture(f"src/main/resources/pokemons/back/${pokemon.id}%03d.png"),
        Layer.Pokemons.layer
    )

    val status: PokemonStatus = PokemonBackStatus(this).moveTo((136, 100))

    def takeDamageAnim(): Unit =
        sprite.translate(
            (-10, 0),
            3,
            () => sprite.translate((10, 0), 3)
        )
