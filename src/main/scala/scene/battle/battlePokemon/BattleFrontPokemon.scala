package Scene.Battle
package BattlePokemon

import Engine.Objects.{GameObject, Sprite}

import Pokemon.Pokemon

import PokemonStatus.{PokemonStatus, PokemonFrontStatus}

import sfml.graphics.Texture

class BattleFrontPokemon(pokemon: Pokemon) extends BattlePokemon(pokemon):
    val sprite: GameObject = GameObject().moveTo((160, 38))
    sprite.addComponent[Sprite](
        Texture(f"src/main/resources/pokemons/front/${pokemon.id}%03d.png"),
        Layer.Pokemons.layer
    )

    val status: PokemonStatus = PokemonFrontStatus(this).moveTo((0, 22))

    def takeDamageAnim(): Unit =
        sprite.translate(
            (10, 0),
            3,
            () => sprite.translate((-10, 0), 3)
        )
