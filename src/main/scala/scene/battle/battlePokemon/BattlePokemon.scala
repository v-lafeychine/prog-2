package Scene.Battle
package BattlePokemon

import Engine.Objects.{GameObject, Sprite}

import Pokemon.{Capacity, Pokemon}

import PokemonStatus.PokemonStatus

import sfml.graphics.Texture

trait BattlePokemon(val pokemon: Pokemon) extends GameObject:
    def attack(enemyPokemon: BattlePokemon, capacity: Capacity): Unit =
        pokemon.useCapacity(capacity, enemyPokemon.pokemon)
        enemyPokemon.status.healthBar.update()
        enemyPokemon.takeDamageAnim()
        println(s"${pokemon.name} attacked ${enemyPokemon.pokemon.name} with ${capacity.name}")

    val sprite: GameObject

    val status: PokemonStatus

    def takeDamageAnim(): Unit
