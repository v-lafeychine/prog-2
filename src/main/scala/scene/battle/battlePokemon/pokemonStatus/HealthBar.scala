package Scene.Battle
package BattlePokemon
package PokemonStatus

import Engine.GameEngine
import Engine.Components.MonoBehaviour
import Engine.Objects.{GameObject, Sprite}

import Pokemon.Pokemon

import sfml.graphics.{FloatRect, IntRect, Texture}
import sfml.system.Vector2f

class HealthBar(pokemon: BattlePokemon) extends GameObject:
    private val sprite =
        addComponent[Sprite](Texture("src/main/resources/battle/health_bar_green.png"), Layer.UI.offset(1))
    private val boundsSprite = FloatRect()
    private var previousLife = pokemon.pokemon.life

    def update(): Unit =
        addComponent[HealthBarScript]()

    private class HealthBarScript() extends MonoBehaviour():
        def healthPercentage(life: Float): Float =
            (life / pokemon.pokemon.maxLife) * (boundsSprite.width / globalScale)

        val rect: IntRect = IntRect(0, 0, 0, (sprite.bounds.height / globalScale).toInt)

        var currentFrame = 0

        var delta = (pokemon.pokemon.life - previousLife).toFloat / 10

        def init(engine: GameEngine): Unit =
            if boundsSprite == FloatRect() then boundsSprite := sprite.bounds

        def update(engine: GameEngine): Unit =
            currentFrame = currentFrame + 1

            rect.width = healthPercentage(previousLife + delta * currentFrame).toInt
            sprite.rect := rect

            if currentFrame == 10 || rect.width < 0 then
                rect.width = healthPercentage(pokemon.pokemon.life).toInt
                previousLife = pokemon.pokemon.life
                destroy()
