package Scene.Battle
package BattlePokemon
package PokemonStatus

import Engine.Objects.{GameObject, Sprite, Text}

import sfml.graphics.{Font, Texture}

class PokemonBackStatus(pokemon: BattlePokemon) extends PokemonStatus(pokemon):
    addComponent[Sprite](Texture("src/main/resources/battle/bar.png"), Layer.UI.layer)

    text.moveTo((20, 6))
    healthBar.moveTo((62, 19))

    List((43, 35), (43, 50)).zip(statusGameObject).foreach((pos, gameObject) => gameObject.moveTo(pos))

    addChild(GameObject().moveTo((95, 26)))
        .addComponent[Text](Font("src/main/resources/font.ttf"), pokemon.pokemon.maxLife.toString, Layer.UI.offset(1))
