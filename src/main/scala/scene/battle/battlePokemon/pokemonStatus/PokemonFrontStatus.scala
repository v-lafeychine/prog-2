package Scene.Battle
package BattlePokemon
package PokemonStatus

import Engine.Objects.Sprite

import sfml.graphics.Texture

class PokemonFrontStatus(pokemon: BattlePokemon) extends PokemonStatus(pokemon):
    addComponent[Sprite](Texture("src/main/resources/battle/bar_enemy.png"), Layer.UI.layer)

    text.moveTo((8, 6))
    healthBar.moveTo((50, 19))

    List((5, 37), (37, 37)).zip(statusGameObject).foreach((pos, gameObject) => gameObject.moveTo(pos))
