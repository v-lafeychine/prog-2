package Scene.Battle
package BattlePokemon
package PokemonStatus

import Engine.Objects.{GameObject, Sprite, Text}

import sfml.graphics.{Font, Texture}
import sfml.system.Vector2f

trait PokemonStatus(pokemon: BattlePokemon) extends GameObject:
    val healthBar = addChild(HealthBar(pokemon))

    val text = addChild(GameObject())
    text.addComponent[Text](Font("src/main/resources/font.ttf"), pokemon.pokemon.name, Layer.UI.offset(1))

    val statusGameObject = pokemon.pokemon.ptype.map(statusType =>
        addChild(GameObject(scale = 0.5))
            .addComponent[Sprite](TextureType.Type.fromOrdinal(statusType.ordinal).texture, Layer.UI.offset(1))
            .gameObject
    )

object TextureType:
    enum Type(val texture: Texture):
        case Bug extends Type(Texture("src/main/resources/battle/types/bug.png"))
        case Dragon extends Type(Texture("src/main/resources/battle/types/dragon.png"))
        case Electric extends Type(Texture("src/main/resources/battle/types/electric.png"))
        case Fight extends Type(Texture("src/main/resources/battle/types/fight.png"))
        case Fire extends Type(Texture("src/main/resources/battle/types/fire.png"))
        case Flying extends Type(Texture("src/main/resources/battle/types/flying.png"))
        case Ghost extends Type(Texture("src/main/resources/battle/types/ghost.png"))
        case Grass extends Type(Texture("src/main/resources/battle/types/grass.png"))
        case Ground extends Type(Texture("src/main/resources/battle/types/ground.png"))
        case Ice extends Type(Texture("src/main/resources/battle/types/ice.png"))
        case Normal extends Type(Texture("src/main/resources/battle/types/normal.png"))
        case Poison extends Type(Texture("src/main/resources/battle/types/poison.png"))
        case Psychic extends Type(Texture("src/main/resources/battle/types/psychic.png"))
        case Rock extends Type(Texture("src/main/resources/battle/types/rock.png"))
        case Water extends Type(Texture("src/main/resources/battle/types/water.png"))
