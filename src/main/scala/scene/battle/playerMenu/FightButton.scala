package Scene.Battle
package PlayerMenu

import Engine.GameEngine
import Engine.Components.MouseCollider
import Engine.Objects.{Button, GameObject, Text, Translation}

import sfml.graphics.{Color, FloatRect, Font, Texture}

class FightButton(var overlay: GameObject) extends Button(Texture("src/main/resources/battle/fight_box.png"), Layer.UI.offset(1)):
    addComponent[FightButtonScript](bounds)

    val text = addChild(GameObject().moveTo(15, 20))
        .addComponent[Text](Font("src/main/resources/font.ttf"), "FIGHT", Layer.UI.offset(2))
    text.color := Color.White()

    private class FightButtonScript(bounds: FloatRect) extends MouseCollider(bounds):
        def onMouseEnter(engine: GameEngine) = ()

        def onMouseClick(engine: GameEngine) =
            active = false
            overlay.translate((0, 192), 8)

        def onMouseExit(engine: GameEngine) = ()
