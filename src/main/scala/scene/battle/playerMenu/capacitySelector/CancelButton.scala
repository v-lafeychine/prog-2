package Scene.Battle
package PlayerMenu
package CapacitySelector

import Engine.GameEngine
import Engine.Components.MouseCollider
import Engine.Objects.{Button, GameObject, Text, Translation}

import sfml.graphics.{Color, FloatRect, Font, Texture}

class CancelButton extends Button(Texture("src/main/resources/battle/cancel_box.png"), Layer.Overlay.offset(1)):
    addComponent[CancelButtonScript](bounds)

    val text = addChild(GameObject().moveTo(15, 20))
        .addComponent[Text](Font("src/main/resources/font.ttf"), "CANCEL", Layer.UI.offset(2))
    text.color := Color.White()

    private class CancelButtonScript(bounds: FloatRect) extends MouseCollider(bounds):
        def onMouseEnter(engine: GameEngine) = ()

        def onMouseClick(engine: GameEngine) =
            active = false
            gameObject.parent.translate(
                (0, -192),
                8,
                () => engine.currentScene.recursiveComponents[MouseCollider]().foreach(_.active = true)
            )

        def onMouseExit(engine: GameEngine) = ()
