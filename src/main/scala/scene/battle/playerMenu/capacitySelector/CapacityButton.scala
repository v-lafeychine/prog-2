package Scene.Battle
package PlayerMenu
package CapacitySelector

import Engine.GameEngine
import Engine.Components.MouseCollider
import Engine.Objects.{Button, GameObject, Sprite, Text}

import Pokemon.Capacity

import protocol.{BattleAttackRequest, Request}

import sfml.graphics.{FloatRect, Font, Texture}

class CapacityButton(capacity: Capacity)
    extends Button(TextureCapacityType.Type.fromOrdinal(capacity.ctype.ordinal).texture, Layer.Overlay.offset(1)):

    addComponent[CapacityButtonScript](bounds)

    addChild(GameObject())
        .moveTo((15, 15))
        .addComponent[Text](Font("src/main/resources/font.ttf"), capacity.name, Layer.Overlay.offset(2))

    addChild(GameObject())
        .moveTo((91, 34))
        .addComponent[Text](Font("src/main/resources/font.ttf"), capacity.maxUses.toString, Layer.Overlay.offset(2))

    val usesLeft = addChild(GameObject())
        .moveTo((71, 29))
        .addComponent[Text](Font("src/main/resources/font.ttf"), capacity.usesLeft.toString, Layer.Overlay.offset(2))

    private class CapacityButtonScript(bounds: FloatRect) extends MouseCollider(bounds):
        def onMouseEnter(engine: GameEngine) = ()

        def onMouseClick(engine: GameEngine) =
            engine.currentScene.recursiveComponents[MouseCollider]().foreach(_.active = false)

            val callback = () => usesLeft.msg = capacity.usesLeft.toString
            gameObject.parent.translate((0, -192), 8, callback)

            engine.sendRequest(
                Request().withBattleAttack(
                    BattleAttackRequest(capacity.toProtobuf)
                )
            )

        def onMouseExit(engine: GameEngine) = ()

object TextureCapacityType:
    enum Type(val texture: Texture):
        case Bug extends Type(Texture("src/main/resources/battle/capacity_types/bug.png"))
        case Dragon extends Type(Texture("src/main/resources/battle/capacity_types/dragon.png"))
        case Electric extends Type(Texture("src/main/resources/battle/capacity_types/electric.png"))
        case Fight extends Type(Texture("src/main/resources/battle/capacity_types/fight.png"))
        case Fire extends Type(Texture("src/main/resources/battle/capacity_types/fire.png"))
        case Flying extends Type(Texture("src/main/resources/battle/capacity_types/flying.png"))
        case Ghost extends Type(Texture("src/main/resources/battle/capacity_types/ghost.png"))
        case Grass extends Type(Texture("src/main/resources/battle/capacity_types/grass.png"))
        case Ground extends Type(Texture("src/main/resources/battle/capacity_types/ground.png"))
        case Ice extends Type(Texture("src/main/resources/battle/capacity_types/ice.png"))
        case Normal extends Type(Texture("src/main/resources/battle/capacity_types/normal.png"))
        case Poison extends Type(Texture("src/main/resources/battle/capacity_types/poison.png"))
        case Psychic extends Type(Texture("src/main/resources/battle/capacity_types/psychic.png"))
        case Rock extends Type(Texture("src/main/resources/battle/capacity_types/rock.png"))
        case Water extends Type(Texture("src/main/resources/battle/capacity_types/water.png"))
