package Scene.Battle
package PlayerMenu
package CapacitySelector

import Engine.Game
import Engine.Objects.{GameObject, Scene, Sprite}

import Pokemon.Pokemon

import sfml.graphics.Texture

class CapacitySelectorOverlay(pokemon: Pokemon) extends GameObject:
    val position = List((2, 6), (130, 6), (2, 67), (130, 67))

    addComponent[Sprite](Texture("src/main/resources/battle/background_capacities.png"), Layer.Overlay.layer)

    addChild(CancelButton()).moveTo((9, 145))

    position
        .zip(pokemon.capacity)
        .foreach((pos, capacity) => addChild(CapacityButton(capacity)).moveTo(pos))
