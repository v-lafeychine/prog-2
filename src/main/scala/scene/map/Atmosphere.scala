package Scene.Map

enum Atmosphere(val season: Season, val time: Time):
    case SummerDay extends Atmosphere(Season.Summer, Time.Day)
    case SummerNight extends Atmosphere(Season.Summer, Time.Night)
    case WinterDay extends Atmosphere(Season.Winter, Time.Day)
    case WinterNight extends Atmosphere(Season.Winter, Time.Night)
    def changeSeason(): Atmosphere =
        this match
            case SummerDay   => WinterDay
            case SummerNight => WinterNight
            case WinterDay   => SummerDay
            case WinterNight => SummerNight

    def changeTime(): Atmosphere =
        this match
            case SummerDay   => SummerNight
            case SummerNight => SummerDay
            case WinterDay   => WinterNight
            case WinterNight => WinterDay

enum Season:
    case Summer, Winter

enum Time:
    case Day, Night
