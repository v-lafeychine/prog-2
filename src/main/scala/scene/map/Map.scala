package Scene.Map

import Character.Character

import Engine.GameEngine
import Engine.Components.{EventListener, KeyListener, RendererLayer}
import Engine.Objects.{GameObject, Sprite}

import Pokemon.{PokemonFactory, Trainer}

import protocol.{Event, SpawnCharacterEvent}
import scala.collection.mutable.ListBuffer

import sfml.graphics.Color
import sfml.system.Vector2i

import scala.io.Source

import sfml.window.Keyboard.Key

class Map(fileName: String, var atmosphere: Atmosphere) extends GameObject:
    private val lines = Source.fromFile("src/main/resources/tiles/" + fileName + ".txt").getLines.toList
    private val tileMap: ListBuffer[MapTile] = ListBuffer()
    private val characters: ListBuffer[Character] = ListBuffer()

    val tileHeight = lines(0).toInt
    val tileWidth = lines(1).toInt
    val tilePixels = lines(2).toInt

    for (line, i) <- lines.drop(3).zipWithIndex do {
        for (letter, j) <- line.toList.zipWithIndex do {
            tileMap += addChild(MapTileFactory(letter)).moveTo((j * tilePixels, i * tilePixels))
        }
    }

    addComponent[CharacterEvent]()
    addComponent[ChangeSeason]()

    def addCharacter(character: Character, initialPosition: Vector2i): Character =
        character.moveTo(initialPosition)

        val child = addChild(character).mapMoveTo(initialPosition)
        characters += child
        child

    private class CharacterEvent extends EventListener():
        def handleEvent(engine: GameEngine, event: Event): Unit =
            event match
                case SpawnCharacterEvent(Some(protocol.Character(name, skin, position, pokemons, _)), _) =>
                    addCharacter(
                        Scene.Map.Character.Character(name, pokemons, skin),
                        (position.get.x, position.get.y)
                    )

                case _ => ()

    private class ChangeSeason extends KeyListener:
        private def updateSeason(season: Season) =
            season match
                case Season.Summer =>
                    tileMap.map(tile => {
                        tile.summerSprite.active = true
                        tile.winterSprite.active = false
                        tile.snow.active = false
                    })
                case Season.Winter =>
                    tileMap.map(tile => {
                        tile.summerSprite.active = false
                        tile.winterSprite.active = true
                        tile.snow.active = true
                    })

        private def updateTime(time: Time) =
            time match
                case Time.Day =>
                    characters.map(character => {
                        character.lightSprite.active = false
                    })
                    tileMap.map(tile => {
                        tile.summerSprite.color := Color(255, 255, 255)
                        tile.winterSprite.color := Color(255, 255, 255)
                        tile.snow.color := Color(255, 255, 255)
                    })
                case Time.Night =>
                    characters.map(character => {
                        character.lightSprite.active = true
                    })
                    tileMap.map(tile => {
                        tile.summerSprite.color := Color(75, 75, 75)
                        tile.winterSprite.color := Color(75, 75, 75)
                        tile.snow.color := Color(75, 75, 75)
                    })

        def handleKeyEvent(engine: GameEngine, key: Key): Unit =
            key match
                case Key.KeySpace => atmosphere = atmosphere.changeSeason(); updateSeason(atmosphere.season)
                case Key.KeyEnter => atmosphere = atmosphere.changeTime(); updateTime(atmosphere.time)
                case _            => ()
