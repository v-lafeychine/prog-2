package Scene.Map

import Character.{Character, Player}

import Engine.GameEngine
import Engine.Components.{EventListener, MonoBehaviour, RendererLayer}
import Engine.Objects.{GameObject, Sprite, Scene}

import Scene.Battle.BattleScene

import protocol.{BattleStartedEvent, Event}

import scala.collection.mutable.ListBuffer

import sfml.graphics.{IntRect, Texture}
import sfml.system.Vector2f

class MapScene(
    player: protocol.Character,
    characters: Seq[protocol.Character],
    fileName: String = "garden",
    atmosphere: Atmosphere = Atmosphere.SummerDay
) extends Scene:
    val map = addGameObject(Scene.Map.Map(fileName, atmosphere))
    private val playerCharacter = map.addCharacter(Player(player.name, player.pokemon, player.skin), (player.position.get.x, player.position.get.y))

    addComponent[FocusCharacter](playerCharacter)
    addComponent[BattleSceneEvent](playerCharacter)

    characters.map(character =>
        map.addCharacter(
            Scene.Map.Character.Character(character.name, character.pokemon, character.skin),
            (character.position.get.x, character.position.get.y)
        )
    )

    val position: ListBuffer[Vector2f] = ListBuffer()

    for x <- List.range(0, 800, 20) do {
        for y <- List.range(0, 800, 20) do {
            position += Vector2f(x, y)
        }
    }

    private class FocusCharacter(character: Character) extends MonoBehaviour(10):
        def init(engine: GameEngine) = ()

        def update(engine: GameEngine) =
            val result = Vector2f()
            val characterPos = character.pos * 2

            val width = 1024
            val height = 768

            if characterPos.y < (height / 2) then ()
            else if characterPos.y > (map.tileHeight * map.tilePixels) - (height / 2) then result.y = (map.tileHeight * map.tilePixels) - height
            else result.y = characterPos.y - height / 2

            if characterPos.x < (width / 2) then ()
            else if characterPos.x > (map.tileWidth * map.tilePixels) - (width / 2) then result.x = (map.tileWidth * map.tilePixels) - width
            else result.x = characterPos.x - width / 2

            moveTo(result * -1)

    private class BattleSceneEvent(player: Character) extends EventListener:
        private def launchBattle(attacker: protocol.Character, isAttacker: Boolean): BattleScene =
            val character = Scene.Map.Character.Character(attacker.name, attacker.pokemon, attacker.skin)

            BattleScene(player.trainer, character.trainer, isAttacker)

        def handleEvent(engine: GameEngine, event: Event): Unit =
            event match
                case BattleStartedEvent(Some(attacker), Some(attacked), _) =>
                    if player.trainer.name == attacker.name then engine.changeScene(launchBattle(attacked, true))
                    else if player.trainer.name == attacked.name then engine.changeScene(launchBattle(attacker, false))

                case _ => ()
