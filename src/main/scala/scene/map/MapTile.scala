package Scene.Map

import Engine.Components.{MonoBehaviour, RendererLayer}
import Engine.GameEngine
import Engine.Objects.{GameObject, Sprite}

import sfml.graphics.{FloatRect, IntRect, Texture}

private class MapTile(summerTexture: Texture, winterTexture: Texture) extends GameObject:
    val summerSprite = addComponent[Sprite](summerTexture, RendererLayer(0))
    val winterSprite = addComponent[Sprite](winterTexture, RendererLayer(0))
    winterSprite.active = false

    val snow = addComponent[Sprite](Texture("src/main/resources/atmosphere/snow.png"), RendererLayer(100))
    snow.rect := IntRect(0, 0, 40, 40)
    snow.active = false

    addComponent[Snow]()

    private class Snow() extends MonoBehaviour(3):
        var frame = 7
        var x = 0

        def init(engine: GameEngine) = ()

        def update(engine: GameEngine) =
            frame = frame - 1

            if frame == 0 then
                x = x + 40
                x = x % 640
                snow.rect := IntRect(600 - x, 0, 40, 40)
                frame = 7

object MapTileFactory:
    class BrickTile extends MapTile(Texture("src/main/resources/tiles/summer/B.png"), Texture("src/main/resources/tiles/winter/B.png"))
    class ConcreteTile extends MapTile(Texture("src/main/resources/tiles/summer/C.png"), Texture("src/main/resources/tiles/winter/C.png"))
    class FlowerTile extends MapTile(Texture("src/main/resources/tiles/summer/F.png"), Texture("src/main/resources/tiles/winter/F.png"))
    class GrassTile extends MapTile(Texture("src/main/resources/tiles/summer/G.png"), Texture("src/main/resources/tiles/winter/G.png"))
    class PathTile extends MapTile(Texture("src/main/resources/tiles/summer/P.png"), Texture("src/main/resources/tiles/winter/P.png"))
    class StatueTile extends MapTile(Texture("src/main/resources/tiles/summer/S.png"), Texture("src/main/resources/tiles/winter/S.png"))
    class TreeTile extends MapTile(Texture("src/main/resources/tiles/summer/T.png"), Texture("src/main/resources/tiles/winter/T.png"))
    class WaterTile extends MapTile(Texture("src/main/resources/tiles/summer/W.png"), Texture("src/main/resources/tiles/winter/W.png"))

    def apply(letter: Char): MapTile =
        letter match
            case 'B' => BrickTile()
            case 'C' => ConcreteTile()
            case 'F' => FlowerTile()
            case 'G' => GrassTile()
            case 'P' => PathTile()
            case 'S' => StatueTile()
            case 'T' => TreeTile()
            case 'W' => WaterTile()
