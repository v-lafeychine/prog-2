package Scene.Map
package Character

import Engine.GameEngine
import Engine.Components.{EventListener, MonoBehaviour, RendererLayer}
import Engine.Objects.{GameObject, Sprite}

import Pokemon.{PokemonFactory, Trainer}

import protocol.{Event, MoveCharacterEvent, DespawnCharacterEvent}

import sfml.graphics.{Color, IntRect, Texture}
import sfml.system.{Vector2f, Vector2i}

class Character(val trainer: Trainer, val characterTexture: CharacterTexture) extends GameObject(scale = 2):
    val mapPosition = Vector2i()

    private val sprite = addChild(GameObject().moveTo(2, 0)).addComponent[Sprite](characterTexture.front, RendererLayer(5))
    sprite.rect := IntRect(0, 0, 16, 20)

    private val light = addChild(GameObject().moveTo(-100 + 8, -100 + 12))
    val lightSprite = light.addComponent[Sprite](Texture("src/main/resources/atmosphere/light.png"), RendererLayer(3))
    lightSprite.color := Color(150, 150, 150)
    lightSprite.active = false

    addComponent[CharacterEvent]()

    def this(name: String, pkmn: protocol.Pokemon, skin: protocol.Skin) =
        this(Trainer(name, PokemonFactory(pkmn)), CharacterTexture(skin))

    def mapMove(direction: Direction): Unit =
        val animation = addComponent[AnimationCharacter]()

        sprite.setTexture(direction.texture)
        mapPosition := mapPosition + direction.move
        translate(direction.move * 20, 3, () => animation.destroy())

    def mapMoveTo(pos: Vector2f): this.type =
        mapPosition := pos
        super.moveTo(pos * 20)

    private class AnimationCharacter() extends MonoBehaviour():
        var i: Int = 0

        def init(engine: GameEngine) =
            i = 0

        def update(engine: GameEngine) =
            i = i + 1
            sprite.animationNumber = i

    private class CharacterEvent() extends EventListener():
        def handleEvent(engine: GameEngine, event: Event): Unit =
            event match
                case DespawnCharacterEvent(name, _) =>
                    if name == trainer.name then gameObject.parent.removeChild(gameObject)

                case MoveCharacterEvent(name, direction, isBlocked, _) =>
                    if name == trainer.name then
                        if isBlocked then sprite.setTexture(Direction(direction, characterTexture).texture)
                        else mapMove(Direction(direction, characterTexture))

                case _ => ()
