package Scene.Map
package Character

import sfml.graphics.Texture

class CharacterTexture(val front: Texture, val back: Texture, val left: Texture, val right: Texture):
    front.repeated = true
    back.repeated = true
    left.repeated = true
    right.repeated = true

    def this(texturePath: String) =
        this(
            Texture("src/main/resources/characters/" + texturePath + "_front.png"),
            Texture("src/main/resources/characters/" + texturePath + "_back.png"),
            Texture("src/main/resources/characters/" + texturePath + "_left.png"),
            Texture("src/main/resources/characters/" + texturePath + "_right.png")
        )

    def this(skin: protocol.Skin) =
        this(skin match
            case protocol.Skin.BOY  => "player1"
            case protocol.Skin.GIRL => "player2"
            case _                  => "player1"
        )
