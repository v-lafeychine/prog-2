package Scene.Map
package Character

import sfml.graphics.Texture
import sfml.system.Vector2i

enum Direction(val texture: Texture, val move: Vector2i):
    case Up(characterTexture: CharacterTexture) extends Direction(characterTexture.back, (0, -1))
    case Down(characterTexture: CharacterTexture) extends Direction(characterTexture.front, (0, 1))
    case Left(characterTexture: CharacterTexture) extends Direction(characterTexture.left, (-1, 0))
    case Right(characterTexture: CharacterTexture) extends Direction(characterTexture.right, (1, 0))

object Direction:
    def apply(direction: protocol.Direction, texture: CharacterTexture): Direction =
        direction match
            case protocol.Direction.UP    => Up(texture)
            case protocol.Direction.DOWN  => Down(texture)
            case protocol.Direction.LEFT  => Left(texture)
            case protocol.Direction.RIGHT => Right(texture)
            case _                        => Up(texture)
