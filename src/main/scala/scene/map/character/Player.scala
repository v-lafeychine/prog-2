package Scene.Map
package Character

import Engine.GameEngine
import Engine.Components.KeyListener
import Engine.Objects.Translation

import Pokemon.{PokemonFactory, Trainer}

import sfml.window.Keyboard.Key

class Player(trainer: Trainer, characterTexture: CharacterTexture) extends Character(trainer, characterTexture):
    def this(name: String, pkmn: protocol.Pokemon, skin: protocol.Skin) =
        this(Trainer(name, PokemonFactory(pkmn)), CharacterTexture(skin))

    addComponent[MovePlayer]()

    private class MovePlayer extends KeyListener:
        def handleKeyEvent(engine: GameEngine, key: Key): Unit =
            if !(getComponents[Translation]().isEmpty) then return ()

            key match
                case Key.KeyUp    => queryMove(engine, protocol.Direction.UP)
                case Key.KeyDown  => queryMove(engine, protocol.Direction.DOWN)
                case Key.KeyLeft  => queryMove(engine, protocol.Direction.LEFT)
                case Key.KeyRight => queryMove(engine, protocol.Direction.RIGHT)
                case _            => ()

        private def queryMove(engine: GameEngine, direction: protocol.Direction): Unit =
            engine.sendRequest(protocol.Request().withMovePlayer(protocol.MovePlayerRequest(direction)), msg => println(msg))
