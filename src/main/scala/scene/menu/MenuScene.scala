package Scene.Menu

import Engine.Components.{KeyListener, MouseCollider, RendererLayer}
import Engine.GameEngine
import Engine.ResponseStatus.{FailureTransfer, Error, Success}
import Engine.Objects.{Button, GameObject, Scene, Sprite, Text}

import Scene.Map.MapScene

import protocol.*

import scalanative.unsigned.UnsignedRichInt

import sfml.graphics.{Color, FloatRect, Font, Texture}
import sfml.network.{IpAddress, SocketStatus}
import sfml.system.Vector2f
import sfml.window.Keyboard.Key

class MenuScene() extends Scene:
    private var ip = ""
    private var name = ""

    private val textState = addText("", (50, 150))
    private val textIPAsk = addText("Welcome, please enter the server IP:", (50, 250))
    private val textIPEnter = addText("", (200, 350))
    private val textNameAsk = addText("Enter your name:", (50, 250), false)
    private val textNameEnter = addText(name, (50, 320), false)
    private val textSkinChoice = addText("Choose your character:", (230, 300), false)

    addComponent[EnterIP]()

    private def addText(msg: String, pos: Vector2f, active: Boolean = true): Text =
        val text = addGameObject(GameObject().moveTo(pos)).addComponent[Text](Font("src/main/resources/font.ttf"), msg, RendererLayer(3))

        text.color := Color.White()
        text.active = active
        return text

    private class EnterIP extends KeyListener:
        private def changeString(str: String): Unit =
            if str.length != 17 then ip = str
            textIPEnter.msg = ip

        def handleKeyEvent(engine: GameEngine, key: Key): Unit =
            key match
                case Key.KeyNum0      => changeString(ip.concat("0"))
                case Key.KeyNum1      => changeString(ip.concat("1"))
                case Key.KeyNum2      => changeString(ip.concat("2"))
                case Key.KeyNum3      => changeString(ip.concat("3"))
                case Key.KeyNum4      => changeString(ip.concat("4"))
                case Key.KeyNum5      => changeString(ip.concat("5"))
                case Key.KeyNum6      => changeString(ip.concat("6"))
                case Key.KeyNum7      => changeString(ip.concat("7"))
                case Key.KeyNum8      => changeString(ip.concat("8"))
                case Key.KeyNum9      => changeString(ip.concat("9"))
                case Key.KeyPeriod    => changeString(ip.concat("."))
                case Key.KeyBackspace => if ip.length == 0 then ip else changeString(ip.substring(0, ip.length - 1))
                case Key.KeyEnter =>
                    textIPAsk.active = false
                    textIPEnter.active = false
                    textState.msg = "Connecting..."
                    engine.connect(IpAddress(ip), 55001.toUShort) match
                        case Left(error) =>
                            textIPAsk.active = true
                            textIPEnter.active = true
                            textState.msg = error

                        case Right(_) =>
                            textNameAsk.active = true
                            textNameEnter.active = true
                            textState.msg = "Successfully connected!"
                            engine.currentScene.addComponent[EnterName]()
                            this.destroy()

                case _ => ()

    private class EnterName extends KeyListener:
        private def changeString(str: String): Unit =
            if str.length != 33 then name = str
            textNameEnter.msg = name

        def handleKeyEvent(engine: GameEngine, key: Key): Unit =
            val letter = (('a'.toInt + key.value).toChar).toString;

            if Key.KeyA.value <= key.value && key.value <= Key.KeyZ.value then changeString(name.concat(letter))
            else if key == Key.KeyBackspace then { if name.length == 0 then ip else changeString(name.substring(0, name.length - 1)) }
            else if key == Key.KeyEnter then {
                engine.sendRequest(
                    Request().withPlayerConnection(ExistingPlayerData(name)),
                    data =>
                        data match
                            case FailureTransfer(error) => textState.msg = error
                            case Error(protocol.Error.UNKNOWN_PLAYER) =>
                                textNameAsk.active = false
                                textNameEnter.active = false
                                textSkinChoice.active = true
                                engine.currentScene.addChild(SkinChoiceButton(Skin.BOY)).moveTo((60, 80)).scale = 5
                                engine.currentScene.addChild(SkinChoiceButton(Skin.GIRL)).moveTo((111, 80)).scale = 5
                            case Error(error) => println("Error: " + error)
                            case Success(response) =>
                                response match
                                    case PlayerConnectionResponse(player, others, _) => engine.changeScene(MapScene(player.get, others))
                                    case _                                           => ()
                )
            }

    private class SkinChoiceButton(skin: protocol.Skin) extends Button(Texture("src/main/resources/skin_choice/skin_button.png"), RendererLayer(5)):

        addChild(GameObject())
            .moveTo((9, 13))
            .addComponent[Sprite](
                Texture(
                    "src/main/resources/skin_choice/" +
                        (skin match
                            case Skin.BOY  => "player1_thumbnail"
                            case Skin.GIRL => "player2_thumbnail"
                            case _         => "player1_thumbnail"
                        )
                        + ".png"
                ),
                RendererLayer(5)
            )

        addComponent[SkinChoiceButtonScript](bounds)

        private class SkinChoiceButtonScript(bounds: FloatRect) extends MouseCollider(bounds):
            def onMouseEnter(engine: GameEngine) = ()

            def onMouseClick(engine: GameEngine) =
                engine.currentScene.recursiveChildren().foreach(_.active = false)
                textSkinChoice.active = false
                val textPokemonChoice = addText("Choose your pokemon:", (230, 250))
                engine.currentScene.addChild(PokemonChoiceButton(skin, Pokemon.BULBASAUR)).moveTo((40, 100)).scale = 3
                engine.currentScene.addChild(PokemonChoiceButton(skin, Pokemon.CHARMANDER)).moveTo((140, 100)).scale = 3
                engine.currentScene.addChild(PokemonChoiceButton(skin, Pokemon.SQUIRTLE)).moveTo((240, 100)).scale = 3
                engine.currentScene.addChild(PokemonChoiceButton(skin, Pokemon.CLEFAIRY)).moveTo((40, 175)).scale = 3
                engine.currentScene.addChild(PokemonChoiceButton(skin, Pokemon.KANGASKHAN)).moveTo((140, 175)).scale = 3
                engine.currentScene.addChild(PokemonChoiceButton(skin, Pokemon.SCYTHER)).moveTo((240, 175)).scale = 3
            def onMouseExit(engine: GameEngine) = ()

    private class PokemonChoiceButton(skin: protocol.Skin, pokemon: protocol.Pokemon)
        extends Button(Texture("src/main/resources/pokemons/void.png"), RendererLayer(5)):

        addChild(GameObject())
            .addComponent[Sprite](
                Texture(
                    "src/main/resources/pokemons/front/" +
                        (pokemon match
                            case Pokemon.BULBASAUR  => "001"
                            case Pokemon.CHARMANDER => "004"
                            case Pokemon.SQUIRTLE   => "007"
                            case Pokemon.CLEFAIRY   => "035"
                            case Pokemon.KANGASKHAN => "115"
                            case Pokemon.SCYTHER    => "123"
                            case _                  => "001"
                        )
                        + ".png"
                ),
                RendererLayer(5)
            )

        addComponent[PokemonChoiceButtonScript](bounds)

        private class PokemonChoiceButtonScript(bounds: FloatRect) extends MouseCollider(bounds):
            def onMouseEnter(engine: GameEngine) = ()

            def onMouseClick(engine: GameEngine) =
                engine.sendRequest(
                    Request().withPlayerConnection(NewPlayerData(name, skin, pokemon)),
                    data =>
                        data match
                            case FailureTransfer(error) => textState.msg = error
                            case Error(error)           => println("Error: " + error)
                            case Success(response) =>
                                response match
                                    case PlayerConnectionResponse(player, characters, _) =>
                                        engine.changeScene(MapScene(player.get, characters))
                                    case _ => ()
                )

            def onMouseExit(engine: GameEngine) = ()
