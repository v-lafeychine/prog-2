package server

import Pokemon.{CapacityFactory, Pokemon, PokemonFactory}

import protocol.{BattleAttackEvent, BattleEndedEvent, Capacity, Error}

class BattleInstance(val initialCharacterAttacker: GameCharacter, val initialCharacterAttacked: GameCharacter):
    private class BattleCharacter(val character: GameCharacter, val pokemon: Pokemon)

    private var attacker = BattleCharacter(initialCharacterAttacker, PokemonFactory(initialCharacterAttacker.pokemon))
    private var attacked = BattleCharacter(initialCharacterAttacked, PokemonFactory(initialCharacterAttacked.pokemon))

    def currentAttackerName: String = attacker.character.name

    def handleAttack(gameServer: GameServer, socketRequest: TcpSocketRequest, capacity: Capacity): Unit =
        findCapacity(attacker.pokemon, capacity) match
            case None =>
                socketRequest.sendError(Error.BATTLE_UNKNOWN_CAPACITY)

            case Some(pkmnCapacity) =>
                if pkmnCapacity.isUsable then
                    val damage = attacker.pokemon.useCapacity(pkmnCapacity, attacked.pokemon)

                    if !(attacked.pokemon.isAlive) then
                        gameServer.sendEvent(BattleEndedEvent(attacker.character.name, attacked.character.name))
                        gameServer.removeBattle(this)
                    else
                        gameServer.sendEvent(BattleAttackEvent(attacker.character.name, attacked.character.name, capacity, damage))
                        newTurn()
                else socketRequest.sendError(Error.BATTLE_UNUSABLE_CAPACITY)

    private def findCapacity(pokemon: Pokemon, capacity: Capacity): Option[Pokemon.Capacity] =
        pokemon.capacity.find(_.name == CapacityFactory(capacity).name)

    private def newTurn(): Unit =
        val tmp = attacker

        attacker = attacked
        attacked = tmp
