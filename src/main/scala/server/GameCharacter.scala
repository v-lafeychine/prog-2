package server

import protocol.{Character, Direction, Position, Pokemon, Skin}

import sfml.network.TcpSocket
import sfml.system.Vector2i

val availableSpawn = List(
    (25, 4),
    (30, 4),
    (8, 10),
    (20, 11),
    (10, 17),
    (14, 18),
    (27, 10),
    (28, 10),
    (45, 11),
    (45, 18),
    (48, 18)
)

class GameCharacter(val name: String, val skin: Skin, val pokemon: Pokemon):
    private var lastMove = System.nanoTime()

    var socket: Option[TcpSocket] = None

    var directionCharacter: Direction = Direction.DOWN
    val position: Vector2i = scala.util.Random.shuffle(availableSpawn).head

    def isExhausted: Boolean =
        System.nanoTime() - lastMove < 180 * 1000 * 1000

    def move(direction: Direction): Unit =
        directionCharacter = direction
        lastMove = System.nanoTime()
        direction match
            case Direction.UP    => position := position + (0, -1)
            case Direction.DOWN  => position := position + (0, 1)
            case Direction.LEFT  => position := position + (-1, 0)
            case Direction.RIGHT => position := position + (1, 0)
            case _               => position := position + (0, -1)

    def toProtobuf: Character =
        Character(name, skin, Some(toProtobufPosition), pokemon)

    def toProtobufPosition: Position =
        Position(position.x, position.y)
