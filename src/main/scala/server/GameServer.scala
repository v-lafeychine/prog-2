package server

import protocol.{BattleStartedEvent, BattleEndedEvent, DespawnCharacterEvent, Direction, Event, MoveCharacterEvent, ServerData}

import scala.collection.mutable.ListBuffer
import scala.io.Source

import sfml.network.TcpSocket

class GameServer(fileName: String = "garden"):
    private val characters: ListBuffer[GameCharacter] = ListBuffer()
    private val battles: ListBuffer[BattleInstance] = ListBuffer()

    private val map: ListBuffer[ListBuffer[Boolean]] = loadMap(fileName)

    private def loadMap(fileName: String): ListBuffer[ListBuffer[Boolean]] =
        val map: ListBuffer[ListBuffer[Boolean]] = ListBuffer()
        val lines = Source.fromFile("src/main/resources/tiles/" + fileName + ".txt").getLines.toList

        for line <- lines.drop(3) do {
            val lineBuffer: ListBuffer[Boolean] = ListBuffer()

            for letter <- line.toList do {
                lineBuffer += (letter == 'B' || letter == 'F' || letter == 'G')
            }
            map += lineBuffer
        }

        return map

    def activeCharacters: Seq[GameCharacter] =
        characters.filter(_.socket.nonEmpty).toList

    def addCharacter(character: GameCharacter): GameCharacter =
        characters += character
        character

    def findBattle(name: String): Option[BattleInstance] =
        battles.find(battle => (battle.initialCharacterAttacker.name == name) || (battle.initialCharacterAttacked.name == name))

    def findCharacter(name: String): Option[GameCharacter] =
        activeCharacters.find(_.name == name)

    def findCharacter(socket: TcpSocket): Option[GameCharacter] =
        activeCharacters.find(_.socket == Some(socket))

    def movePlayer(socketRequest: TcpSocketRequest, player: GameCharacter, direction: Direction): Unit =
        if player.isExhausted then return ()

        val queryPosition = direction match
            case Direction.UP    => player.position + (0, -1)
            case Direction.DOWN  => player.position + (0, 1)
            case Direction.LEFT  => player.position + (-1, 0)
            case Direction.RIGHT => player.position + (1, 0)
            case _               => player.position + (0, -1)

        activeCharacters.filter(_.position == queryPosition).headOption match
            case Some(character) =>
                findBattle(character.name) match
                    case None =>
                        battles += BattleInstance(player, character)
                        player.directionCharacter = direction
                        sendEvent(BattleStartedEvent(Some(player.toProtobuf), Some(character.toProtobuf)))

                    case Some(_) =>
                        player.directionCharacter = direction
                        sendEvent(MoveCharacterEvent(player.name, direction, true))

            case None =>
                if map(queryPosition.y)(queryPosition.x) then
                    player.move(direction)
                    sendEvent(MoveCharacterEvent(player.name, direction, false))
                else if player.directionCharacter != direction then
                    player.directionCharacter = direction
                    sendEvent(MoveCharacterEvent(player.name, direction, true))

    def removeBattle(battle: BattleInstance): Unit =
        battles -= battle

    def removeConnection(socketRequest: TcpSocketRequest): Unit =
        findCharacter(socketRequest.socket) match
            case None => ()
            case Some(character) =>
                findBattle(character.name).map(battle =>
                    sendEvent(
                        BattleEndedEvent(
                            if character.name == battle.initialCharacterAttacker.name then battle.initialCharacterAttacker.name
                            else battle.initialCharacterAttacked.name,
                            character.name
                        )
                    )
                    battles -= battle
                )
                sendEvent(DespawnCharacterEvent(character.name))
                character.socket = None

    def sendEvent(event: Event): Unit =
        activeCharacters.map(_.socket.get.send(ServerData().withEvent(event).toByteArray))
