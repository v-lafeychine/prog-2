package server

import protocol.{ClientData, Request}

import scala.collection.mutable.ListBuffer

import sfml.network.{IpAddress, SocketStatus, SocketSelector, TcpListener, TcpSocket}

class Listener(val listener: TcpListener):
    private val sockets: ListBuffer[TcpSocket] = ListBuffer()
    private val selector = SocketSelector()

    def pollRequest(): LazyList[(TcpSocketRequest, Option[Request])] =
        def polling(): List[(TcpSocketRequest, Option[Request])] =
            selector.clear()
            selector.add(listener)

            sockets.foreach(selector.add(_))

            if selector.wait() then
                if selector.isReady(listener) then handleNewConnection()
                else return handleSocketsPackets()

            return polling()

        LazyList.continually(polling()).flatten

    private def handleNewConnection(): Unit =
        listener.accept() match
            case SocketStatus.Done(socket) =>
                println(s"[INFO] New connection ${socket.remoteAddress}:${socket.remotePort}")
                sockets += socket

            case _ => ()

    private def handleSocketsPackets(): List[(TcpSocketRequest, Option[Request])] =
        val disconnectedSockets: ListBuffer[TcpSocket] = ListBuffer()
        val requestSockets: ListBuffer[(TcpSocketRequest, Option[Request])] = ListBuffer()

        for socket <- sockets do
            if selector.isReady(socket) then
                socket.receive() match
                    case SocketStatus.Disconnected() =>
                        requestSockets += ((TcpSocketRequest(socket, 0), None))
                        socket.close()
                        disconnectedSockets += socket

                    case SocketStatus.Done(data) =>
                        try {
                            val clientData = ClientData.parseFrom(data)

                            requestSockets += ((TcpSocketRequest(socket, clientData.id), Some(clientData.request.get)))
                        } catch case _ => println(s"[WARN] Bad data recieved from ${socket}")

                    case _ => ()

        sockets --= disconnectedSockets
        return requestSockets.toList
