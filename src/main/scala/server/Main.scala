package server

import protocol.*

import scalanative.unsigned.{UnsignedRichInt, UShort}

import sfml.network.{SocketStatus, TcpListener}

@main def main() =
    val listener = Listener(TcpListener())
    val gameServer = GameServer()

    listener.listener.listen(55001.toUShort) match
        case SocketStatus.Done(_) => ()
        case _ =>
            println("[ERROR] Cannot start the server")
            sys.exit(1)

    println(s"[INFO] Starting on ${listener.listener.localPort}")

    for (socketRequest, request) <- listener.pollRequest() do
        request match
            case None => gameServer.removeConnection(socketRequest)

            case Some(request) =>
                request.requestType match
                    case Request.RequestType.PlayerConnection(playerConnection) =>
                        handlePlayerConnection(socketRequest, playerConnection)

                    case Request.RequestType.MovePlayer(MovePlayerRequest(direction, _)) =>
                        getPlayer(socketRequest).map(player =>
                            gameServer.findBattle(player.name) match
                                case None    => gameServer.movePlayer(socketRequest, player, direction)
                                case Some(_) => socketRequest.sendError(Error.PLAYER_MOVE_IN_BATTLE)
                        )

                    case Request.RequestType.BattleAttack(BattleAttackRequest(capacity, _)) =>
                        getPlayer(socketRequest).map(player =>
                            getBattle(socketRequest, player).map(battle =>
                                if battle.currentAttackerName == player.name then battle.handleAttack(gameServer, socketRequest, capacity)
                                else socketRequest.sendError(Error.BATTLE_NOT_YOUR_TURN)
                            )
                        )

                    case Request.RequestType.SyncToMap(_) =>
                        getPlayer(socketRequest).map(player =>
                            socketRequest.sendResponse(
                                SyncToMapResponse(Some(player.toProtobuf), gameServer.activeCharacters.map(_.toProtobuf))
                            )
                        )

                    case _ => ()

    def addConnection(socketRequest: TcpSocketRequest, character: GameCharacter): Unit =
        socketRequest.sendResponse(PlayerConnectionResponse(Some(character.toProtobuf), gameServer.activeCharacters.map(_.toProtobuf)))
        gameServer.sendEvent(SpawnCharacterEvent(Some(character.toProtobuf)))
        character.socket = Some(socketRequest.socket)

    def handlePlayerConnection(socketRequest: TcpSocketRequest, playerConnection: PlayerConnectionRequest): Unit =
        playerConnection match
            case ExistingPlayerData(name, _) =>
                gameServer.findCharacter(name) match
                    case None => socketRequest.sendError(Error.UNKNOWN_PLAYER)
                    case Some(character) =>
                        character.socket match
                            case Some(_) => socketRequest.sendError(Error.SOCKET_ALREADY_CONNECTED)
                            case None    => addConnection(socketRequest, character)

            case NewPlayerData(name, skin, pkmn, _) =>
                gameServer.findCharacter(name) match
                    case Some(_) => socketRequest.sendError(Error.NAME_ALREADY_TAKEN)
                    case None    => addConnection(socketRequest, gameServer.addCharacter(GameCharacter(name, skin, pkmn)))

            case _ => ()

    def getBattle(socketRequest: TcpSocketRequest, character: GameCharacter): Option[BattleInstance] =
        gameServer.findBattle(character.name) match
            case Some(battle) => Some(battle)
            case None =>
                socketRequest.sendError(Error.PLAYER_NOT_IN_BATTLE)
                None

    def getPlayer(socketRequest: TcpSocketRequest): Option[GameCharacter] =
        gameServer.findCharacter(socketRequest.socket) match
            case Some(character) => Some(character)
            case None =>
                socketRequest.sendError(Error.SOCKET_NO_CONNECTED)
                None
