package server

import protocol.{Event, Error, Response, ResponseData, ServerData}

import sfml.network.TcpSocket

class TcpSocketRequest(val socket: TcpSocket, idRequest: Long):
    def sendError(error: Error): Unit =
        socket.send(ServerData().withResponseData(ResponseData(idRequest).withError(error)).toByteArray)

    def sendResponse(response: Response): Unit =
        socket.send(ServerData().withResponseData(ResponseData(idRequest).withResponse(response)).toByteArray)
