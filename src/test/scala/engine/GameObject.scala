package Engine
package Objects

import org.junit.Assert.{assertEquals, assertSame}
import org.junit.Test

import Engine.Components.Component
import Engine.Objects.GameObject

import sfml.system.Vector2f

class GameObjectTest:
    @Test def parentHierarchy =
        val gameObject = GameObject()
        val subGameObject = gameObject.addChild(GameObject())

        assertSame(gameObject, subGameObject.parent)

    @Test def parentMove =
        val gameObject = GameObject().moveTo(10, 10)
        val subGameObject = gameObject.addChild(GameObject().moveTo(20, 20))

        assertEquals(Vector2f(30, 30), subGameObject.globalPos)

    @Test def parentScaleMove =
        val gameObject = GameObject(scale = 3).moveTo(10, 10)
        val subGameObject = gameObject.addChild(GameObject().moveTo(20, 20))

        assertEquals(Vector2f(70, 70), subGameObject.globalPos)

    @Test def getComponent =
        val gameObject = GameObject()

        val a = gameObject.addComponent[A]()
        val b = gameObject.addComponent[B]()
        val c = gameObject.addComponent[C]()

        assertEquals(List(a, c), gameObject.getComponents[A]().toList)
        assertEquals(List(b), gameObject.getComponents[B]().toList)
        assertEquals(List(c), gameObject.getComponents[C]().toList)

    private class A extends Component
    private class B extends Component
    private class C extends A
