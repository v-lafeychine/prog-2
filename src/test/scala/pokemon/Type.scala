import org.junit.Assert.assertEquals
import org.junit.Test

import Pokemon.*

class TypeTest:
    @Test def strongTypeEfficient =
        assertEquals(Efficient, Fire.typeEfficiency(Grass))

    @Test def weakTypeNotEfficient =
        assertEquals(NotEfficient, Bug.typeEfficiency(Fight))

    @Test def normalTypeNeutral =
        assertEquals(Neutral, Electric.typeEfficiency(Poison))

    @Test def inefficientTypeInefficient =
        assertEquals(Inefficient, Normal.typeEfficiency(Ghost))
